<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.sql.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>选课页面</title>
</head>
<body>
    <%
     request.setCharacterEncoding("UTF-8");
     Class.forName("com.mysql.jdbc.Driver");
     
     String url1="jdbc:mysql://localhost:13306/";
     String dbname="examination_results";
     String url=url1+dbname;
     String user="root";
     String password="1234";
     Connection conn=DriverManager.getConnection(url, user, password);
     
     String Cno=(String)request.getParameter("Cno");
     String Cname=(String)request.getParameter("Cname");
     String Sno=(String)session.getAttribute("Snokk");
     String Sname=(String)session.getAttribute("namekk");
     
     String sql1="select * from stucourse where Sno=? and Cno=?";
     PreparedStatement pst1=conn.prepareStatement(sql1);
     pst1.setString(1, Sno);
     pst1.setString(2, Cno);
     ResultSet rs=pst1.executeQuery();
     if(rs.next())
     {
    	 out.print("该课程已选过！");
     }
     else
     {
    	 String sql="insert into stucourse(Sno,Sname,Cno,Cname) values(?,?,?,?)";
         PreparedStatement pst=conn.prepareStatement(sql);
         
         pst.setString(1, Sno);
         pst.setString(2, Sname);
         pst.setString(3, Cno);
         pst.setString(4, Cname);
         int n=pst.executeUpdate();
         if(n!=0)
         {
        	 out.println("选课成功！");
         }
         else
         {
        	 out.print("选课失败！");
         }
         String sql2="insert into sc(Sno,Sname,Cno,Cname) values(?,?,?,?)";
         PreparedStatement pst2=conn.prepareStatement(sql2);
         
         pst2.setString(1, Sno);
         pst2.setString(2, Sname);
         pst2.setString(3, Cno);
         pst2.setString(4, Cname);
         int n2=pst2.executeUpdate();
         if(n2!=0)
         {
        	 out.println("添加课表成功！");
         }
         else
         {
        	 out.print("添加课表失败！");
         }
     }
     %>

</body>
</html>