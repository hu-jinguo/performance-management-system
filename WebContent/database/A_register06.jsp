<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.sql.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>超级用户登录验证页面</title>
</head>
<body>
  <%
  request.setCharacterEncoding("UTF-8");
  Class.forName("com.mysql.jdbc.Driver");
  
  String url1="jdbc:mysql://localhost:13306/";//MySQL5.7端口号：13306
  String dbname="examination_results";//数据库名称
  String url=url1+dbname;
  String user="root";
  String password="1234";
  Connection conn=DriverManager.getConnection(url, user, password);
  
  String sql1="select * from teacher where Tno=? and Tname=?";
  PreparedStatement pst1=conn.prepareStatement(sql1);
  
  String Tno12=request.getParameter("Tno02");
  String Tname12=request.getParameter("Tname02");
  session.setAttribute("Tnokk", Tno12);
  
  pst1.setString(1, Tno12);
  pst1.setString(2, Tname12);
  ResultSet rs=pst1.executeQuery();
  
  if(rs.next())
  {
	  out.println("欢迎使用！页面即将跳转！");
	  response.setHeader("refresh", "2;tea_index.jsp");//2秒自动刷新并跳转到登录页面
  }else
  {
	  out.println("用户不存在或账号密码错误！");
	  response.setHeader("refresh", "2;A_register05.html");
  }
  
  if(rs!=null)
	  rs.close();
  if(pst1!=null)
	  pst1.close();
  if(conn!=null)
	  conn.close();
  %>

</body>
</html>