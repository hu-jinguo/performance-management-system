<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.sql.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>成绩页面</title>
</head>
<body>
     成绩页面：<hr width="100%" size="3"><br><br><br>
   <%
    request.setCharacterEncoding("UTF-8");
	Class.forName("com.mysql.jdbc.Driver");
	
	String url1="jdbc:mysql://localhost:13306/";
    String dbname="examination_results";
    String url=url1+dbname;
    String user="root";
    String password="1234";
    Connection conn=DriverManager.getConnection(url, user, password);
    String Sno=(String)session.getAttribute("Snokk");
	String sql="select a.Sno,a.Sname,a.Cno,Tname,a.Cname,a.Score from sc a,teacher b,course c where a.Sno=? and a.Cno=c.Cno and c.Tno=b.Tno";
	
	PreparedStatement pst=conn.prepareStatement(sql);
	pst.setString(1, Sno);
	ResultSet rs=pst.executeQuery();
	%>
	
	<center>
	<form action="#">
     <table border="2" bgcolor="ccceee" width="1000">
      <tr bgcolor="CCCCCC" align="center">
        <td>记录条数</td><td>学号</td><td>姓名</td>
        <td>课程号</td><td>教师姓名</td><td>课程名</td><td>成绩</td>
      </tr>
      <%
        while(rs.next())
        {
	      %>
	        <tr align="center">
	          <td><%=rs.getRow() %></td>
	          <td><%=rs.getString("Sno") %></td>
	          <td><%=rs.getString("Sname") %></td>
	          <td><%=rs.getInt("Cno") %></td>
	          <td><%=rs.getString("Tname") %></td>
	          <td><%=rs.getString("Cname") %></td>
	          <td><%=rs.getString("Score") %></td>
	         </tr>    
	  <%} %>
      </table>
   </form>
   </center>
   
   <%
   if(pst!=null) pst.close();
   if(conn!=null) conn.close();
   if(rs!=null) rs.close();
   %>
</body>
</html>