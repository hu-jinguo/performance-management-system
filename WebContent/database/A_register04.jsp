<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.sql.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>普通用户登录验证页面</title>
</head>
<body>
  <%
  request.setCharacterEncoding("UTF-8");
  Class.forName("com.mysql.jdbc.Driver");
  
  String url1="jdbc:mysql://localhost:13306/";//MySQL5.7端口号：13306
  String dbname="examination_results";//数据库名称
  String url=url1+dbname;
  String user="root";
  String password="1234";
  Connection conn=DriverManager.getConnection(url, user, password);
  
  String sql1="select * from suser where root=? and password=?";
  PreparedStatement pst1=conn.prepareStatement(sql1);
  
  String root12=request.getParameter("root02");
  String password12=request.getParameter("password02");
  
  pst1.setString(1, root12);
  pst1.setString(2, password12);
  
  ResultSet rs=pst1.executeQuery();
  
  if(rs.next())
  {
	  String Sno=rs.getString("Sno");
	  session.setAttribute("Snokk", Sno);
	  String name=rs.getString("name");
	  session.setAttribute("namekk", name);
	  out.println("欢迎使用！页面即将跳转！");
	  response.setHeader("refresh", "2;stu_index.jsp");//2秒自动刷新并跳转到登录页面
  }else
  {
	  out.println("用户不存在或账号密码错误！");
	  response.setHeader("refresh", "2;A_register02.html");
  }
  
  if(rs!=null)
	  rs.close();
  if(pst1!=null)
	  pst1.close();
  if(conn!=null)
	  conn.close();
  %>

</body>
</html>
