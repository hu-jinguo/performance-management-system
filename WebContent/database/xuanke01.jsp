<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.sql.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>选课页面</title>
</head>
<body>
    选课页面：<hr width="100%" size="3"><br><br><br>
   <%
    request.setCharacterEncoding("UTF-8");
	Class.forName("com.mysql.jdbc.Driver");
	
	String url1="jdbc:mysql://localhost:13306/";
    String dbname="examination_results";
    String url=url1+dbname;
    String user="root";
    String password="1234";
    Connection conn=DriverManager.getConnection(url, user, password);
	String sql="select a.Cno,a.Cname,a.Tno,b.Tname,a.Credit,a.Ctype from course a,teacher b where a.Tno=b.Tno";
	
	PreparedStatement pst=conn.prepareStatement(sql);
	ResultSet rs=pst.executeQuery();
	%>
	
	<center>
	<form action="#">
     <table border="2" bgcolor="ccceee" width="1000">
      <tr bgcolor="CCCCCC" align="center">
        <td>记录条数</td><td>课程号</td><td>课程名</td>
        <td>教师工号</td><td>教师姓名</td><td>学分</td><td>课程类型</td><td>选课</td>
      </tr>
      <%
        while(rs.next())
        {
	      %>
	        <tr align="center">
	          <td><%=rs.getRow() %></td>
	          <td><%=rs.getString("Cno") %></td>
	          <td><%=rs.getString("Cname") %></td>
	          <td><%=rs.getString("Tno") %></td>
	          <td><%=rs.getString("Tname") %></td>
	          <td><%=rs.getInt("Credit") %></td>
	          <td><%=rs.getString("Ctype") %></td>
	          <td><a href="xuanke02.jsp?Cno=<%=rs.getString("Cno") %>&Cname=<%=rs.getString("Cname") %>">选课</a></td>
	         </tr>    
	  <%} %>
      </table>
   </form>
   </center>
   
   <%
   if(pst!=null) pst.close();
   if(conn!=null) conn.close();
   if(rs!=null) rs.close();
   %>
</body>
</html>