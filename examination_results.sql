/*
SQLyog Ultimate v11.25 (64 bit)
MySQL - 5.7.34-log : Database - examination_results
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`examination_results` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `examination_results`;

/*Table structure for table `course` */

DROP TABLE IF EXISTS `course`;

CREATE TABLE `course` (
  `Cno` char(20) NOT NULL,
  `Cname` char(20) NOT NULL,
  `Tno` char(20) DEFAULT NULL,
  `Credit` int(11) DEFAULT NULL,
  `Ctype` char(20) DEFAULT NULL,
  PRIMARY KEY (`Cno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `course` */

insert  into `course`(`Cno`,`Cname`,`Tno`,`Credit`,`Ctype`) values ('3300','数据结构','1100',3,'必修'),('3301','算法设计','1100',2,'选修'),('3302','设计模式','1100',1,'选修'),('3303','数据库原理','1100',4,'必修');

/*Table structure for table `sc` */

DROP TABLE IF EXISTS `sc`;

CREATE TABLE `sc` (
  `Sno` char(20) NOT NULL,
  `Sname` char(20) DEFAULT NULL,
  `Cno` char(20) NOT NULL,
  `Cname` char(20) DEFAULT NULL,
  `Score` int(11) DEFAULT NULL,
  PRIMARY KEY (`Sno`,`Cno`),
  CONSTRAINT `sc_ibfk_1` FOREIGN KEY (`Sno`, `Cno`) REFERENCES `stucourse` (`Sno`, `Cno`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sc` */

insert  into `sc`(`Sno`,`Sname`,`Cno`,`Cname`,`Score`) values ('123','李华','3300','数据结构',98),('123','李华','3301','算法设计',NULL);

/*Table structure for table `stucourse` */

DROP TABLE IF EXISTS `stucourse`;

CREATE TABLE `stucourse` (
  `Sno` char(20) NOT NULL,
  `Sname` char(20) DEFAULT NULL,
  `Cno` char(20) NOT NULL,
  `Cname` char(20) DEFAULT NULL,
  PRIMARY KEY (`Sno`,`Cno`),
  KEY `Cno` (`Cno`),
  CONSTRAINT `stucourse_ibfk_1` FOREIGN KEY (`Sno`) REFERENCES `student` (`Sno`) ON DELETE CASCADE,
  CONSTRAINT `stucourse_ibfk_2` FOREIGN KEY (`Cno`) REFERENCES `course` (`Cno`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `stucourse` */

insert  into `stucourse`(`Sno`,`Sname`,`Cno`,`Cname`) values ('123','李华','3300','数据结构'),('123','李华','3301','算法设计');

/*Table structure for table `student` */

DROP TABLE IF EXISTS `student`;

CREATE TABLE `student` (
  `Sno` char(20) NOT NULL,
  `Sname` char(20) NOT NULL,
  `Ssex` char(20) DEFAULT NULL,
  `Sage` int(11) DEFAULT NULL,
  `Sdept` char(20) DEFAULT NULL,
  `Sclass` char(20) DEFAULT NULL,
  PRIMARY KEY (`Sno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `student` */

insert  into `student`(`Sno`,`Sname`,`Ssex`,`Sage`,`Sdept`,`Sclass`) values ('123','李华','男',20,'计算机科学','计算机4班');

/*Table structure for table `suser` */

DROP TABLE IF EXISTS `suser`;

CREATE TABLE `suser` (
  `root` varchar(20) NOT NULL,
  `password` varchar(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `Sno` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`root`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `suser` */

insert  into `suser`(`root`,`password`,`name`,`Sno`) values ('test','123','李华','123');

/*Table structure for table `teacher` */

DROP TABLE IF EXISTS `teacher`;

CREATE TABLE `teacher` (
  `Tno` char(20) NOT NULL,
  `Tname` char(20) NOT NULL,
  `Tsex` char(20) DEFAULT NULL,
  `Tage` int(11) DEFAULT NULL,
  `Tdept` char(20) DEFAULT NULL,
  PRIMARY KEY (`Tno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `teacher` */

insert  into `teacher`(`Tno`,`Tname`,`Tsex`,`Tage`,`Tdept`) values ('1100','魏涛明','男',27,'计算机系');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
